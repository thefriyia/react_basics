import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Animated,
  Image,
  Easing
} from 'react-native'

class CicularMovement extends Component {

}

class HorizontalMovement extends Component {

  constructor () {
    super()
    this.animatedValue = new Animated.Value(0)
  }

  componentDidMount () {
    this.animate()
  }

  animate () {
    this.animatedValue.setValue(0)
    Animated.timing(
    this.animatedValue,
    {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear
    }).start(() => this.animate())
  }

  render () {
    const marginLeft = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 300]
    })
    const opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 0]
    })
    const movingMargin = this.animatedValue.interpolate({
      inputRange: [0, .6, 1],
      outputRange: [0, 340, 0]
    })
    const textSize = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [18, 32, 18]
    })
    const rotateX = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: ['0deg', '180deg', '0deg']
    })

    return (
      <View style={styles.container}>
      <Animated.View
        style={{
        marginLeft: movingMargin,
        marginTop: 10,
        height: 30,
        width: 40,
        backgroundColor: 'orange'}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      paddingTop: 150
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'red',
    //marginTop: -50,
  },
  button: {
    backgroundColor: 'black',
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginTop: 15,

  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default class graphics extends Component {
  render() {
    return (
      <HorizontalMovement></HorizontalMovement>
    );
  }
}
AppRegistry.registerComponent('graphics', () => graphics);
