import React, { Component } from 'react';
import { AppRegistry, Text, View, StyleSheet } from 'react-native';

const headerStyle = StyleSheet.create({
  TextStyles: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    margin: 35,
    textAlign: 'center',
  },
  BannerStyles: {
    height: 75,
    backgroundColor: 'darkorange',
  },
});

class Header extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={ headerStyle.BannerStyles }>
          <Text style={ headerStyle.TextStyles }>ULTIMATE FITNESS</Text>
        </View>
        <View style={{ flex: 2, backgroundColor: 'white' }} />
      </View>
   );
  }
}

class FlexDimensions extends Component {
  render() {
    return (
      <Header/>
    );
  }
}

AppRegistry.registerComponent('tutorial', () => FlexDimensions);
